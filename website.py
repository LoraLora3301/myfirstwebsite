#!/usr/bin/python3

from flask import Flask,request,redirect,url_for,render_template

app = Flask(__name__)

SECRET_PASSWORD = "SHIROUSAGI<3"

@app.route("/")
def world():
    content_lines=[ \
        "Personnal website, do not go to /admin route.", \
        "So much secrets there" \
    ]
    return render_template('index.html', content_lines=content_lines)

@app.route("/admin", methods=['GET', 'POST'])
def admin():
    print("YOLO")
    if request.method == 'POST':
        key = request.form.get('key')
        if key == SECRET_PASSWORD:
            return render_template('admin.html')
    elif request.method == 'GET':
        return render_template('admin_login.html')
    return redirect(url_for("world"))
